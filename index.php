<?php include('header.php') ?>
  <?php include_once 'functions.php' ?>
  <main role="main" class="inner cover">
    <div class="col-md-12">
      <form action="functions.php" method="post" name="upload_excel" enctype="multipart/form-data">
          <fieldset>
              <!-- Form Name -->
              <legend>Importar Arquivo de Provisões de gastos</legend>
              <!-- File Button -->
              <div class="form-group">
                <input type="file" name="file" id="file">
              </div>
              <!-- Button -->
              <div class="form-group">
                <button type="submit" id="submit" name="Import" class="btn btn-lg btn-secondary" data-loading-text="Carregando...">Importar</button>
              </div>
          </fieldset>
      </form>
    </div>
    <script type="text/javascript">
    </script>


  </main>
<?php include('footer.php') ?>